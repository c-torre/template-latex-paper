MAIN_NAME = main
SRC = src
BLD = build

MAIN_SRC = ${SRC}/${MAIN_NAME}
MAIN_BLD = ${BLD}/${MAIN_NAME}

MAIN_TEX = ${MAIN_SRC}.tex
MAIN_BIB = ${MAIN_SRC}.bib
